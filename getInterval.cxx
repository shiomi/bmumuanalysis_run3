#include "/home/shiomi/RootUtils/RootUtils/TLegend_addfunc.h"
#include "/home/shiomi/RootUtils/RootUtils/TCanvas_opt.h"
#include "/home/shiomi/RootUtils/AtlasUtils.C"

using namespace RooStats;

void Text(){myTextVer2(0.50, 0.80, 1, "#sqrt{s}=7 TeV, 4.9 fb^{-1}, stat only");}
void Text2(){myTextVer2(0.50, 0.74, 1, "#sqrt{s}=8 TeV, 20 fb^{-1}, stat only");}

void getInterval( 
  const char* infile = "../root/CountingModel_Bmumu_Run1_0205.root",
  const char* workspaceName = "w",
  const char* modelConfigName = "ModelConfig",
  const char* dataName = "data" )
{ 
  // open input file, and get workspace
  TFile *file = TFile::Open(infile);
  RooWorkspace* w = dynamic_cast<RooWorkspace*>(file->Get(workspaceName));

  // get the modelConfig out of the file 
  RooStats::ModelConfig* mc = dynamic_cast<ModelConfig*>(w->obj(modelConfigName));
  
  RooAbsData* data = w->data(dataName);
  
  ProfileLikelihoodCalculator pl_1sig(*data,*mc);
  pl_1sig.SetConfidenceLevel(0.683); // 1σ interval
  LikelihoodInterval* int_1sig = pl_1sig.GetInterval();
  
  ProfileLikelihoodCalculator pl_2sig(*data,*mc);
  pl_2sig.SetConfidenceLevel(0.955); // 2σ interval
  LikelihoodInterval* int_2sig = pl_2sig.GetInterval();

  ProfileLikelihoodCalculator pl_3sig(*data,*mc);
  pl_3sig.SetConfidenceLevel(0.997); // 3σ interval
  LikelihoodInterval* int_3sig = pl_3sig.GetInterval();
  
  // find the iterval on the first Parameter of Interest 
  RooRealVar* firstPOI = dynamic_cast<RooRealVar*>(mc->GetParametersOfInterest()->first());
  
  double low_1sig = int_1sig->LowerLimit(*firstPOI);
  double upp_1sig = int_1sig->UpperLimit(*firstPOI);
  double low_2sig = int_2sig->LowerLimit(*firstPOI);
  double upp_2sig = int_2sig->UpperLimit(*firstPOI);
  double low_3sig = int_3sig->LowerLimit(*firstPOI);
  double upp_3sig = int_3sig->UpperLimit(*firstPOI);
  
  cout << "\n68% interval on " <<firstPOI->GetName()<<" is : ["<< low_1sig << ", "<< upp_1sig <<"] "<<endl;
  cout << "\n95% interval on " <<firstPOI->GetName()<<" is : ["<< low_2sig << ", "<< upp_2sig <<"] "<<endl;
  cout << "\n99% interval on " <<firstPOI->GetName()<<" is : ["<< low_3sig << ", "<< upp_3sig <<"] "<<endl;
  
  LikelihoodIntervalPlot * p_1sig = new LikelihoodIntervalPlot(int_1sig);
  LikelihoodIntervalPlot * p_2sig = new LikelihoodIntervalPlot(int_2sig);
  LikelihoodIntervalPlot * p_3sig = new LikelihoodIntervalPlot(int_3sig);

  TCanvas c1;
  TString pdf("../pdf/GetInterval_Bmumu_Run1_OnlyStat_0127.pdf");
  c1.Print(pdf + "[", "pdf");

  // patch to profile-likelihood plot with systematics
  TF1 * f1 = int_1sig->GetLikelihoodRatio()->asTF(*firstPOI);
  f1->GetYaxis()->SetRangeUser(0,7);
  f1->GetXaxis()->SetRangeUser(1.0,9);
  f1->GetXaxis()->SetTitleOffset(0.95);
  f1->GetYaxis()->SetTitleOffset(0.95);
  f1->GetXaxis()->SetTitleSize(0.06);
  f1->GetYaxis()->SetTitleSize(0.06);
  f1->SetTitle(";BR(B^{0}_{s}#rightarrow#mu#mu) [10^{-9}];-#Deltaln L");
  f1->Draw();
  
  TF1 * f2 = int_3sig->GetLikelihoodRatio()->asTF(*firstPOI);
  f2->SetRange(low_3sig, upp_3sig);
  f2->SetFillColor(kCyan);
  f2->SetFillStyle(1001);
  f2->Draw("SAME FC");

  TF1 * f3 = int_2sig->GetLikelihoodRatio()->asTF(*firstPOI);
  f3->SetRange(low_2sig, upp_2sig);
  f3->SetFillColor(kAzure+10);
  f3->SetFillStyle(1001);
  f3->Draw("SAME FC");
  
  TF1 * f4 = int_1sig->GetLikelihoodRatio()->asTF(*firstPOI);
  f4->SetRange(low_1sig, upp_1sig);
  f4->SetFillColor(kBlue+2);
  f4->SetFillStyle(1001);
  f4->Draw("SAME FC");
  
  TLine l1(3.65,0,3.65,7); l1.SetLineColor(kRed); l1.Draw();
  TBox *b1 = new TBox(3.42,0,3.88,7); b1->SetFillStyle(3345); b1->SetFillColor(kRed); b1->SetLineColor(10); b1->Draw();


  ATLASLabel(0.50,0.88,"Work In Progress");
  Text();
  Text2();
  TLegend* legend =new TLegend(0.50,0.70,0.65,0.63);
  legend->AddEntry(b1,"SM","F");
  legend->SetBorderSize(0);
  legend->SetFillStyle(0);
  legend->Draw("same L");
  
  gPad->RedrawAxis();
  c1.Print(pdf,"pdf");
  c1.Print( pdf + "]", "pdf" );
}
