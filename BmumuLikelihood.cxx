#ifndef __CINT__
#include "RooGlobalFunc.h"
#endif
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooAbsReal.h"
#include "RooPlot.h"
#include "RooWorkspace.h"
#include "RooProdPdf.h"
#include "RooMinuit.h"
#include "RooFitResult.h"
#include "math.h"
#include "/home/shiomi/RootUtils/RootUtils/TLegend_addfunc.h"
#include "/home/shiomi/RootUtils/RootUtils/TCanvas_opt.h"
#include "/home/shiomi/RootUtils/AtlasUtils.C"

using namespace RooFit;

void Text(){myTextVer2(0.55, 0.80, 1, "#sqrt{s}=7 TeV, 4.9 fb^{-1}, stat only");}
void Text2(){myTextVer2(0.55, 0.74, 1, "#sqrt{s}=8 TeV, 20 fb^{-1}, stat only");}

void BmumuLikelihood()
{
  RooWorkspace w("w");

  //BR(Bs->mumu)
  /*
  w.factory("Gaussian::bkg(b0[-5,10],alpha[3.01,-5,10],sigma[1])");
  w.var("b0")->setVal(3.01);
  w.var("b0")->setConstant(true);
  w.var("sigma")->setVal(3.01*0.17);
  */

  w.factory("prod::s(BR[3.65,0,10],alpha[3.01])");

  // Poisson * Gaussian 
  w.factory("Poisson::pdf(obs[0,50],s)");
  w.factory("Gaussian::bkg(b0[0,20],b[11,0,20],sigma[1])");
  w.var("b0")->setVal(11);
  w.var("b0")->setConstant(true);
  w.var("sigma")->setVal(11*0.12);
  w.factory("PROD::model(pdf,bkg)");
  

  //Generate ModelConfig
  RooStats::ModelConfig mc("ModelConfig",&w);
  mc.SetPdf(*w.pdf("model"));
  mc.SetParametersOfInterest(*w.var("BR"));
  w.var("obs")->setVal(11);
  mc.SetObservables(*w.var("obs"));
  mc.SetNuisanceParameters(*w.var("b"));
  mc.SetGlobalObservables(*w.var("b0"));
  mc.SetSnapshot(*w.var("BR")); 

  mc.Print();
  w.import(mc);

  // make data set with the namber of observed events 
  RooDataSet data("data","", *w.var("obs"));
  w.var("obs")->setVal(11);
  data.add(*w.var("obs"));

  // import data set in workspace and save it in a file 
  w.import(data);
  w.Print();

  TString fileName = "../root/CountingModel_Bmumu_Run1_0205.root";
  w.writeToFile(fileName, true);

  //RooDataSet* d_poi = w.pdf("model")->generate(*w.var("obs"),10000);
  RooDataSet* d_poi = w.pdf("model")->generate(RooArgSet(*w.var("obs"),*w.var("b0")),10000);
  //w.pdf("pdf")->fitTo(*d_poi);
  RooAbsReal* nll = w.pdf("model")->createNLL(*d_poi);
  RooAbsReal* pll = nll->createProfile(*w.var("BR"));

  RooPlot* f_poi = w.var("obs")->frame();
  //d_poi->plotOn(f_poi);
  w.pdf("model")->plotOn(f_poi);
  
  RooPlot* f_sys = w.var("b0")->frame();
  w.pdf("model")->plotOn(f_sys);
  
  RooPlot* f_br = w.var("BR")->frame();
  w.pdf("model")->plotOn(f_br);
  
  TH2* h_2d = (TH2*)w.pdf("model")->createHistogram("obs,b0",50,50);
  h_2d->SetLineColor(kAzure);
  
  RooPlot* f_NLL = w.var("BR")->frame(Bins(5),Range((double)2,(double)7),Title("Likelihood"));
  nll->plotOn(f_NLL,ShiftToZero());
  pll->plotOn(f_NLL,LineColor(kRed));

  TCanvas c1;
  TString pdf("../pdf/BmumuLikelihood_Run1_0205.pdf");
  c1.Print(pdf + "[", "pdf");

  f_poi->Draw();
  f_poi->SetTitle(";N_{obs};Events");
  ATLASLabel(0.55,0.88,"Work In Progress");
  Text();
  Text2();
  c1.Print(pdf,"pdf"); c1.Clear();
  f_sys->Draw();
  ATLASLabel(0.55,0.88,"Work In Progress");
  Text();
  Text2();
  c1.Print(pdf,"pdf"); c1.Clear();
  f_br->Draw();
  ATLASLabel(0.55,0.88,"Work In Progress");
  Text();
  Text2();
  c1.Print(pdf,"pdf"); c1.Clear();
  h_2d->Draw("SURF");
  ATLASLabel(0.55,0.88,"Work In Progress");
  Text();
  Text2();
  c1.Print(pdf,"pdf"); c1.Clear();
  f_NLL->Draw();
  ATLASLabel(0.55,0.88,"Work In Progress");
  Text();
  Text2();
  c1.Print(pdf,"pdf"); c1.Clear();

  c1.Print( pdf + "]", "pdf" );
}
